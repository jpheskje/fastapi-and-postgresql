from pydantic import BaseModel
from typing import List, Optional, Union
from datetime import date
from queries.pool import pool

## the BaseModel from pydantic has nothing to do with the db
## is purely related to the data being passed from FastAPI


class Error(BaseModel):
    message: str


class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


class VacationOut(BaseModel):
    id: int
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


class VacationRepository:
    def get_one(self, vacation_id: int) -> Optional[VacationOut]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id
                            , name
                            , from_date
                            , to_date
                            , thoughts
                        FROM vacations
                        WHERE id = %s
                        """,
                        [vacation_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_vacation_out(record)
        except Exception as e:
            print(e)
            return {"message": "could not get the vacation"}

    def delete(self, vacation_id: int) -> bool:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # run our SELECT statement
                    db.execute(
                        """
                        DELETE FROM vacations
                        WHERE id = %s
                        """,
                        [vacation_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(self) -> Union[List[VacationOut], Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    # run our SELECT statement
                    result = db.execute(
                        """
                        SELECT id, name, from_date, to_date, thoughts
                        FROM vacations
                        ORDER BY from_date;
                        """
                    )
                    # result = []
                    # for record in db:
                    #     vacation = VacationOut(
                    #         id=record[0], name=record[1], from_date=record[2], to_date=record[3], thoughts=record[4]
                    #     )
                    #     result.append(vacation)
                    # return result
                    ## below list comprehension is equivalent of above
                    # return [
                    #     VacationOut(
                    #         id=record[0], name=record[1], from_date=record[2], to_date=record[3], thoughts=record[4]
                    #     )
                    #     for record in db
                    # ]
                    ## below using custom function to populate VacationOut
                    return [self.record_to_vacation_out(record) for record in db]
                    record_to_vacation_out
        except Exception as e:
            print(e)
            return {"message": "could not get all vacations"}

    def create(self, vacation: VacationIn) -> VacationOut:
        # connect the database
        ## 'with' is a monitor, lets us use the connection and give it back
        ## prevents us from using several try except blocks
        with pool.connection() as conn:
            # get a curson (somethign to run SQL with)
            with conn.cursor() as db:
                # run INSERT statement
                result = db.execute(
                    """
                    INSERT INTO vacations
                        (name, from_date, to_date, thoughts)
                    VALUES
                        (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [vacation.name, vacation.from_date, vacation.to_date, vacation.thoughts],
                )
                id = result.fetchone()[0]
                # return new data, which will be the same information + an id
                # old_data = vacation.dict()
                # return VacationOut(id=id, **old_data)
                return self.vacation_in_to_out(id, vacation)
                # return {"message": "error"}

    def update(self, vacation_id: int, vacation: VacationIn) -> Union[VacationOut, Error]:
        try:
            # connect the database
            with pool.connection() as conn:
                # get a cursor (something to run SQL with)
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE vacations
                        SET name = %s
                          , from_date = %s
                          , to_date = %s
                          , thoughts = %s
                        WHERE id = %s
                        """,
                        [vacation.name, vacation.from_date, vacation.to_date, vacation.thoughts, vacation_id],
                    )
                    # old_data = vacation.dict()
                    # return VacationOut(id=vacation_id, **old_data)
                    return self.vacation_in_to_out(vacation_id, vacation)
        except Exception as e:
            print(e)
            return {"message": "could not update vacation"}

    def vacation_in_to_out(self, id: int, vacation: VacationIn) -> VacationOut:
        old_data = vacation.dict()
        return VacationOut(id=id, **old_data)

    def record_to_vacation_out(self, record):
        return VacationOut(id=record[0], name=record[1], from_date=record[2], to_date=record[3], thoughts=record[4])
